﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public interface IGemItem
{
    int Id { get; }
    void SetViewData(Sprite sprite, Color color);
    void AssignListeners(Func<object, EventArgs, bool> gemClicked);
    void Select();
    void Deselect();
}

public class GemTable : MonoBehaviour
{
    [Serializable]
    private class GemType
    {
        public string Name = "";
        public Sprite Sprite = default;
        public Color Color = default;
    }

    [SerializeField]
    private GameObject m_gemPrefab = default;
    [SerializeField]
    private Transform m_pooledCage = default;
    [SerializeField]
    private GridPacker m_gridPacker = default;
    [SerializeField]
    private GemType[] m_gemTypes = default;
    [SerializeField]
    private GridSettings m_gridSettings = default;

    private ItemPool m_pool = default;
    private IGemItem[,] m_gems = default;
    private GameObject[,] m_objs = default;
    private IGemItem m_selectedGem = default;
    private bool m_isInitiated = false;

    private void Start()
    {
        SceneControl.Instance.ActivateGameView += Initiate;
    }

    private void Initiate()
    {
        if (m_isInitiated)
            return;

        if (m_gridSettings == null)
            throw new ArgumentNullException(nameof(m_gridSettings));
        if (m_gemPrefab == null)
            throw new ArgumentNullException(nameof(m_gemPrefab));
        if (m_gemPrefab.GetComponent<IGridCell>() == null || m_gemPrefab.GetComponent<IGemItem>() == null)
            throw new InvalidCastException($"Prefabs doesn't implement expected interfaces: {nameof(IGridCell)}, {nameof(IGemItem)}");

        m_pool = new ItemPool(m_gemPrefab, m_pooledCage);

        SetupGems();

        m_gridPacker.ArrangeItems(m_gridSettings, m_objs);

        m_isInitiated = true;
    }

    private void SetupGems()
    {
        m_gems = new IGemItem[m_gridSettings.CountH, m_gridSettings.CountV];
        m_objs = new GameObject[m_gridSettings.CountH, m_gridSettings.CountV];
        var goHeap = m_pool.Take(m_gridSettings.TotalCount);
        var usedTypeIndexes = new int[m_gridSettings.CountH, m_gridSettings.CountV];
        var allTypeIndexes = Enumerable.Range(0, m_gemTypes.Length).ToList();
        var filteredTypeIndexes = new List<int>();

        for (int v = 0; v < m_gridSettings.CountV; v++)
        {
            for (int h = 0; h < m_gridSettings.CountH; h++)
            {
                // определить набор вероятных типов, исключив серию одинаковых типов более 2ух подряд с ближайшими соседними ячейками слева и сверху
                int previousType = -1;
                filteredTypeIndexes.Clear();
                filteredTypeIndexes.AddRange(allTypeIndexes);
                if (h > 1)
                {
                    previousType = usedTypeIndexes[h - 1, v];
                    if(previousType == usedTypeIndexes[h - 2, v])
                        filteredTypeIndexes.Remove(previousType);
                }

                if (v > 1)
                {
                    previousType = usedTypeIndexes[h, v - 1];
                    if (previousType == usedTypeIndexes[h, v - 2])
                        filteredTypeIndexes.Remove(previousType);
                }

                var typeIndex = filteredTypeIndexes[UnityEngine.Random.Range(0, filteredTypeIndexes.Count)];
                var type = m_gemTypes[typeIndex];

                var gobj = goHeap[v * m_gridSettings.CountH + h];
                var gem = gobj.GetComponent<IGemItem>();

                gem.AssignListeners(GemClickedListener);
                gem.SetViewData(type.Sprite, type.Color);

                m_gems[h, v] = gem;
                m_objs[h, v] = gobj;
                usedTypeIndexes[h, v] = typeIndex;
            }
        }
    }

    private void BurnCombos()
    {
        for (int v = 0; v < m_gridSettings.CountV; v++)
        {
            //var row = m_gridControl.Cells
        }
    }

    private bool GemClickedListener(object sender, EventArgs args)
    {
        if (sender != null || sender is IGemItem)
        {
            var gem = (IGemItem)sender;

            m_selectedGem?.Deselect();
            m_selectedGem = m_selectedGem == null ? gem : m_selectedGem.Id == gem.Id ? null : gem;
            m_selectedGem?.Select();

            return true;
        }
        else
        {
            throw new Exception($"Unexpected type of sender: {sender}");
        }

    }
}
