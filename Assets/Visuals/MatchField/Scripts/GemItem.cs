﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using Random = UnityEngine.Random;

public class GemItem : MonoBehaviour, IGridCell, IGemItem
{
    [SerializeField]
    private SpriteRenderer m_spriteRenderer = default;
    [SerializeField]
    private GameObject m_highlightObject = default;
    [SerializeField]
    private TextMeshPro m_debugCounter = default;

    private Func<object, EventArgs, bool> m_itemClickedMethod = default;

    public int CellId { get; private set; } = 0;
    public int CellMarkerH { get; private set; } = 0;
    public int CellMarkerV { get; private set; } = 0;

    int IGemItem.Id { get { return CellId; } }

    void IGridCell.SetGridMarkers(int markerX, int markerY)
    {
        CellMarkerH = markerX;
        CellMarkerV = markerY;
        CellId = 1000 * markerX + markerY;
    }

    void IGemItem.SetViewData(Sprite sprite, Color color)
    {
        m_spriteRenderer.sprite = sprite;
        m_spriteRenderer.color = color;
    }

    void IGemItem.AssignListeners(Func<object, EventArgs, bool> gemClicked)
    {
        m_itemClickedMethod = gemClicked;
    }

    void IGemItem.Select()
    {
        m_highlightObject.SetActive(true);
    }

    void IGemItem.Deselect()
    {
        m_highlightObject.SetActive(false);
    }

    private void OnMouseDown()
    {
        m_itemClickedMethod(this, EventArgs.Empty);
    }
}
