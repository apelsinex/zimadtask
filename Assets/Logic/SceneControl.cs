﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class SceneControl : MonoBehaviour
{
    public Action ActivateGameView = delegate { };

    static public SceneControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = new GameObject("SceneControl").AddComponent<SceneControl>();

            return _instance;
        }
    }

    static private SceneControl _instance = default;

    [SerializeField]
    private GemTable m_itemPrefab;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError($"Another {nameof(SceneControl)} already exist, this duplicate will be destroyed.");
            DestroyImmediate(this);
        }
    }

    private void Start()
    {
        ActivateGameView?.Invoke();
    }
}
