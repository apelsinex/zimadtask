﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static IEnumerable<T> DequeueRange<T>(this Queue<T> queue, int range)
    {
        for (int i = 0; i < range && queue.Count > 0; i++)
        {
            yield return queue.Dequeue();
        }
    }

    public static IEnumerable<T> PopRange<T>(this Stack<T> queue, int range)
    {
        for (int i = 0; i < range && queue.Count > 0; i++)
        {
            yield return queue.Pop();
        }
    }
}
