﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public interface IGridCell
{
    int CellId { get; }
    int CellMarkerH { get; }
    int CellMarkerV { get; }
    void SetGridMarkers(int coordX, int coordY);
}

[Serializable]
public class GridSettings
{
    public int CountH = 1;
    public int CountV = 1;
    public Vector2 CellSize = Vector2.one;

    public int TotalCount { get { return CountH * CountV; } }
}

public class GridPacker : MonoBehaviour
{
    private GridSettings m_settings = default;
    private GameObject[,] m_items = default;
    private IGridCell[,] m_cells = default;
    private bool m_arranged = false;

    public void ArrangeItems(GridSettings settings, GameObject[,] items)
    {
        if (m_arranged)
            return;

        CheckSettings(settings);
        m_settings = settings;

        CheckItems(items);
        m_items = items;

        RepositionItems();

        m_arranged = true;
    }

    private void RepositionItems()
    {
        m_cells = new IGridCell[m_settings.CountH, m_settings.CountV];
        var gridSize = new Vector2(m_settings.CountH * m_settings.CellSize.x, m_settings.CountV * m_settings.CellSize.y);
        var startOffset = -0.5f * gridSize + m_settings.CellSize * 0.5f;

        for (int v = 0; v < m_settings.CountV; v++)
        {
            for (int h = 0; h < m_settings.CountH; h++)
            {
                var gobj = m_items[h, v];
                gobj.transform.SetParent(transform);
                gobj.transform.localPosition = new Vector3(startOffset.x + h * m_settings.CellSize.x, startOffset.y + v * m_settings.CellSize.y, 0f);
                gobj.SetActive(true);

                var cell = gobj.GetComponent<IGridCell>();
                cell.SetGridMarkers(h, v);
                m_cells[h, v] = cell;

#if UNITY_EDITOR
                gobj.name = $"Cell_{cell.CellId}";
#endif
            }
        }
    }

    private void CheckSettings(GridSettings settings)
    {
        if (settings == null)
            throw new ArgumentNullException(nameof(settings));
        else if (settings.CountH <= 0 || settings.CountV <= 0)
            throw new ArgumentException($"Vertical or horizontal cell count has invalid value, expected 1 or greater.");
        else if (settings.CellSize.magnitude == 0f || settings.CellSize.x <= 0f || settings.CellSize.y <= 0f)
            throw new ArgumentException($"Cell size equals zero or has negative values.", nameof(settings.CellSize));
    }

    private void CheckItems(GameObject[,] items)
    {
        if (items.Length == 0)
            throw new ArgumentException($"Passed empty array as income parameter.", nameof(items));
        if (items.GetLength(0) != m_settings.CountH || items.GetLength(1) != m_settings.CountV)
            throw new ArgumentException($"Array's size differs from grid settings.", nameof(items));
    }
}
