﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ItemPool
{
    private Stack<GameObject> m_stack = new Stack<GameObject>();
    private GameObject m_prefab = default;
    private Transform m_cage = default;

    public ItemPool(GameObject prefab, Transform cage)
    {
        m_prefab = prefab;
        m_cage = cage;
    }

    public List<GameObject> Take(int count)
    {
        var availableCount = m_stack.Count;
        var createCount = count - availableCount;

        if (createCount > 0)
        {
            for (int i = 0; i < createCount; i++)
            {
                var newItem = UnityEngine.Object.Instantiate(m_prefab, m_cage);
                newItem.SetActive(false);
                m_stack.Push(newItem);
            }
        }

        return m_stack.PopRange(count).ToList();
    }

    public void Stash(List<GameObject> items)
    {
        if (items == null)
            throw new ArgumentNullException(nameof(items));

        for (int i = 0; i < items.Count; i++)
        {
            var item = items[i];
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            item.transform.SetParent(m_cage);
            item.SetActive(false);
            m_stack.Push(item);
        }
    }
}
